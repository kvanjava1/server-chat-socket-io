const app = require('express')();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const serverChat = require('./src/controllers/ServerChat');
const middlewareAuth = require('./src/middlewares/Authorization');

server.listen(3000);

middlewareAuth.run(io);
serverChat.run(io);



