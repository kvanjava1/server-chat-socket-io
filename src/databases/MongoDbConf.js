var MongoClient = require('mongodb').MongoClient

exports.getConn = async () => {
    return await MongoClient.connect("mongodb://localhost:27017/mydatabase", { useNewUrlParser: true });
}
