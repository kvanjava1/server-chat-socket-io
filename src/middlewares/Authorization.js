const jwt = require('jsonwebtoken');

exports.run = (io) => {	

	io.use(function(socket, next){
	  	
	  	var decoded;	
		var authorization = socket.handshake.query['Authorization'];
		var err  = new Error('error');
  		
		// validation data
		if(authorization == null){
			
			err.data = { 
				response: 'response_authorization',
				status: false,
				message: 'fail authorization' 
			};			
			
			return next(err);
		
		}

		try{
			
			decoded = jwt.verify(authorization, 'example_key');

			if (decoded.valid != true) {

				err.data = { 
					response: 'response_authorization',
					status: false,
					message: 'fail authorization' 
				};			

				return next(err);
			
			}

			socket.userProfile = decoded;

		}catch(errAuth) {
			
			err.data = { 
				response: 'response_authorization',
				status: false,
				message: errAuth.message 
			};		

			return next(err);
		}

		return next();
	})

}