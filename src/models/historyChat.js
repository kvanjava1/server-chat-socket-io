// const connection = require('./../database/MysqlConf');
const connection = require('./../databases/MongoDbConf');
const datetime = require('node-datetime');
const db = 'kuka_history_chat_v3';
const cltHistoryChat = 'history_chat';
const cltHistoryChatLog = 'history_chat_log';


exports.addHistoryChat = async (fromUserId, fromName, toUserId, toName) => {
  try {      
    var client = await connection.getConn();
    let dbSelected = client.db(db);
    var closeTryCatch = false;

    let qHistoryChat = { 
                          $and: [
                                  {"from.user_id": parseInt(fromUserId) }, 
                                  {"to.user_id": parseInt(toUserId) }
                                ] 
                        };

    dbSelected.collection(cltHistoryChat).findOne(qHistoryChat, function(err, result) {

        if (result == null) {

          let dt = datetime.create();
          let id = dt.format('YmdHMS');

          let historyData = {
                              _id: parseInt(id),
                              from: {
                                user_id: parseInt(fromUserId),
                                name: fromName
                              },
                              to: {
                                user_id: parseInt(toUserId),
                                name: toName
                              },
                              has_unread_message: 0
                            }

          dbSelected.collection(cltHistoryChat).insertOne(historyData); 
          client.close();

        }

        closeTryCath = true;

    });
    
  }catch(err){
    console.log("ERROR SYSTEM HISTORY");
  }finally{

    if (closeTryCatch) {
      client.close();
    }
  
  }
}

exports.addHistoryChatLog = async (fromUserId, toUserId, message, messageId, askProduct, product, createAt, sentStatus) => {    
  try {      
    var client = await connection.getConn();
    var dbSelected = client.db(db);
    
    var dt = datetime.create();
    var id = dt.format('YmdHMS');
    
    var dataHistoryLog = {
                            _id: parseInt(id),
                            from: {
                              user_id: parseInt(fromUserId)
                            },
                            to: {
                              user_id: parseInt(toUserId)
                            },
                            message: message,
                            message_id: parseInt(messageId),
                            ask_product: askProduct,
                            product: product,
                            created_at: createAt,
                            is_sent: sentStatus,
                            is_read: false
                         }
                         
    dbSelected.collection(cltHistoryChatLog).insertOne(dataHistoryLog);

  }catch(err){
    console.log("ERROR_SYSTEM ADD HISTORY CHAT LOG");
  }finally {
    client.close();
  }
}

exports.getHistoryChat = async (userId) => {
  
  try {
    
    var client = await connection.getConn();
    var dbSelected = client.db(db);   

    var qHistoryChat = { 
                          $or: [
                                  {"from.user_id": parseInt(userId) }, 
                                  {"to.user_id": parseInt(userId) }
                                ] 
                        };

    // hitory ke siapa aja pernah ngobrol
    var resultHistoryChat = await dbSelected.collection(cltHistoryChat).find(qHistoryChat).sort( { _id: -1 } ).toArray();
    var cleanHistoryChat = {};
    var next = false;

    // bersihkan history 
    // from priority
    for(let key1 in resultHistoryChat){
    
      if (parseInt(resultHistoryChat[key1].from.user_id) == parseInt(userId)) {

        next = false;
        for(let key2 in cleanHistoryChat){
          if (parseInt(cleanHistoryChat[key2].user_id) == parseInt(resultHistoryChat[key1].to.user_id)) {
            next = true;
            break;
          }
        }
        
        if (next == false) {
          cleanHistoryChat[key1] = resultHistoryChat[key1].to;
        }else{
          next = false;
        }

      }

    }

    // to priority
    for(let key1 in resultHistoryChat){
    
      if(parseInt(resultHistoryChat[key1].to.user_id) == parseInt(userId)){
        
        next = false;
        for(let key2 in cleanHistoryChat){
          if (parseInt(cleanHistoryChat[key2].user_id) == parseInt(resultHistoryChat[key1].from.user_id)) {
            next = true;
            break;
          }
        }
        
        if (next == false) {
          cleanHistoryChat[key1] = resultHistoryChat[key1].from;
        }else{
          next = false;
        }

      }

    }
    
  }catch(err){
    console.log(err.message);
  }finally{
    client.close();
  }

  return cleanHistoryChat;

}

exports.getHistoryChatLog = async (userIdSender , userIdReceiver) => {

  try {
    var client = await connection.getConn();
    var dbSelected = client.db(db);   

    var qHistoryChatLog = { 
                            '$or': 
                                  [ 
                                    { 
                                      '$and': 
                                              [
                                                {"from.user_id" : parseInt(userIdSender)},
                                                {"to.user_id" :  parseInt(userIdReceiver)}
                                              ] 
                                    }, 
                                    { 
                                      '$and': 
                                              [
                                                {"from.user_id" : parseInt(userIdReceiver)},
                                                {"to.user_id" :  parseInt(userIdSender)}
                                              ] 
                                    } 
                                  ] 
                          };

    // hitory ke siapa aja pernah ngobrol
    return dbSelected.collection(cltHistoryChatLog).find(qHistoryChatLog).sort( { _id: 1 } ).toArray();
    
  }catch(err){
    console.log(err.message);
  }finally{
    client.close();
  }

}

exports.getUnreadHistoryChatLog = async (userIdSender , userIdReceiver) => {

  try {
    var client = await connection.getConn();
    var dbSelected = client.db(db);   

    var qHistoryChatLog = { 
                            '$and': 
                                    [
                                      {"from.user_id" : parseInt(userIdSender)},
                                      {"to.user_id" :  parseInt(userIdReceiver)},
                                      {"is_read": false}
                                    ] 
                          };

    // hitory ke siapa aja pernah ngobrol
    return dbSelected.collection(cltHistoryChatLog).find(qHistoryChatLog).sort( { _id: 1 } ).toArray();
    
  }catch(err){
    console.log(err.message);
  }finally{
    client.close();
  }

}

exports.updateUnreadHistoryChatLog = async (userIdSender , userIdReceiver, isRead) => {

  try {
    var client = await connection.getConn();
    var dbSelected = client.db(db);   

    var qHistoryChatLog = { 
                            '$and': 
                                    [
                                      {"from.user_id" : parseInt(userIdSender)},
                                      {"to.user_id" :  parseInt(userIdReceiver)},
                                      {"is_read": false}
                                    ] 
                          };

    var qUpdate = {
      "is_read" : true
    };

    // hitory ke siapa aja pernah ngobrol
    return dbSelected.collection(cltHistoryChatLog).updateMany(qHistoryChatLog, { '$set': qUpdate });
    
  }catch(err){
    console.log(err.message);
  }finally{
    client.close();
  }

}

