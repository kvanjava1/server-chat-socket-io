const jwt = require('jsonwebtoken');
const sanitizeHtml = require('sanitize-html');
const mdlHistoryChat = require('./../models/historyChat');

var listSocketConnected = {};
var socket = null;

exports.run = async (io) => {	

	console.log('-----------------SERVER-RUN-----------------');
	
	io.on('connection', function (cnfgSocket) {

		socket = cnfgSocket;

		// add connection socket
		acceptConnection();
		// handle client request
		handleRequest();		
		// handle disconnect
		handleDisconnect();

	});
}

function handleRequest() {
	
	socket.on('chat_to_shop', function (data) {
		
		if (data.request == 'send_message') { 
			forwardMessage(data);
		}else if (data.request == 'check_user_online') { 	
			isUserOnlineById(data.user_id);
		}else if (data.request == 'history_chat') { 	
			getHistoryChat(data.user_id);
		}else if (data.request == 'history_chat_log') {
			getHistoryChatLog(data.from.user_id, data.to.user_id);
		}else if (data.request == 'read_all_message') {
			setReadAllMessage(data.from.user_id, data.to.user_id);
		}else{
			
			//flash message
			console.log('request unknow');
			console.log(data);
			console.log('force to disconnect ' + socket.id);
			socket.disconnect();

		}

	});
}

function handleDisconnect() {

	socket.on('disconnect', function (socket) {

		var userIdDisconect = '';
		var stillOnline = false;
  		
  		// cari yg mana yg disconect remove from list 
		for (let key in listSocketConnected) {
			
			if(listSocketConnected[key].clientSocket.connected == false){		
	
				// flash message
      			userIdDisconect = listSocketConnected[key].userId;
      			console.log('ada yg disconnect si ' + listSocketConnected[key].nickName);
      			
      			//remove socket
				delete listSocketConnected[key];
			}
		}

		// masih ada userId tersebut ? kalau engga send tokok udah offline
		for (let key in listSocketConnected) {
			
			if(listSocketConnected[key].userId == userIdDisconect){		
				
				stillOnline = true;
				break;
			
			}
		}

		// kalau dia offline sama sekali tidak ada koneksinya kasih tau semua
		if (stillOnline == false) {

			sendToAllConnection({ 
									response: 'broadcast_user_status',
									success: true,
									message:'',
									data: {
											userId: userIdDisconect,
											status: 'offline'
									}
								});

		}

		console.log('Banyak user tersisa ' + Object.keys(listSocketConnected).length);
		showListOnline();
  	
  	});

}

function setReadAllMessage(from, to){

	// validasi authorization pengirim ?
	try {
		to = jwt.verify(to, 'example_key')
	} catch(err) {
		
	 	console.log('fail authorized');
	 	return false;
	
	}
	var conn = getConnectionById(to);
	mdlHistoryChat.updateUnreadHistoryChatLog(from, conn.userId);

}

async function getHistoryChat(id){
	
	var conn = null;

	try {
		id = jwt.verify(id, 'example_key')
	} catch(err) {
	 	console.log('error from get history');
	 	return false;
	}

	conn = getConnectionById(id);

	if (conn == null) {
		console.log('user not found');
		return false;
	}

	
	var listChatWith = await mdlHistoryChat.getHistoryChat(conn.userId);

	for(let key1 in listChatWith){

		let totalUnread = await mdlHistoryChat.getUnreadHistoryChatLog(listChatWith[key1].user_id, conn.userId);
		listChatWith[key1].total_unread = totalUnread.length;

	}

	var response = { 
						response: 'response_history_chat',
						success: true,
						message: '',
						data: {
							from : {
								userId: conn.userId
							},
							list_history_chat : listChatWith	
						}
					}
	
	console.log('-----------------------------------------------');
	console.log('send history chat to ' + conn.userId);

	sendToAllConnectionHasUserId(response, conn.userId);

}

async function acceptConnection(){
	
	// save socket
	listSocketConnected[socket.id] = {
		clientSocket : socket,
		userId : socket.userProfile.iss, // issuer berisi user id
		nickName : socket.userProfile.aud, // name user login
		email : socket.userProfile.email 
	};

	// say hello to client
	console.log('Welcome ' + socket.userProfile.aud);
	console.log('Banyak user yang sedang online ' + Object.keys(listSocketConnected).length);
	console.log('Make ' + socket.userProfile.aud + ' allowed to chat ' + socket.userProfile.iss);

	socket.emit('chat_to_shop', { 
									response: 'response_authorization',
									success: true,
									message:'',
									data: {
											name: socket.userProfile.aud,
											reg_id: jwt.sign(socket.id, 'example_key'),
											message: 'hey welcome to kuka chat anda terdaftar sebagai ' + socket.userProfile.aud
									} 
								});

	showListOnline();
	broadcastUserStatus(socket.userProfile.iss);
	return true;

}

function broadcastUserStatus(userId){

	sendToAllConnection({ 
							response: 'broadcast_user_status',
							status: true,
							message:'',
							data: {
								userId: userId,
								status: 'online'
							}
						});

}

async function getHistoryChatLog(from, to){

	// validasi authorization pengirim ?
	try {
		from = jwt.verify(from, 'example_key')
	} catch(err) {
		
	 	console.log('fail authorized');
	 	return false;
	
	}

	var conn = getConnectionById(from);

	console.log('----------------------------------------------');
	console.log('send history chat log, to ' + conn.userId);

	var historyChatLog = await mdlHistoryChat.getHistoryChatLog(conn.userId, to);

	sendToAllConnectionHasUserId({
		response: 'response_history_chat_log',
		succes: true,
		message: '',
		data : {
			from: {
				user_id: conn.userId
			},
			to: {
				user_id: to
			},
			list_chat_log : historyChatLog	
		}
	}, conn.userId);

}

async function forwardMessage(data){

	// validasi authorization pengirim ?
	try {
		data.from = jwt.verify(data.from, 'example_key')
	} catch(err) {
		
	 	console.log('fail authorized');
	 	return false;
	
	}

	// validasi message
	if ((data.message).length > 100) {
	
		getConnectionById(data.from).clientSocket.disconnect();
		return false;
	
	}

	data.message = sanitizeHtml(data.message);

	// validasi koneksi si penerima ?
	var sckTo = getConnectionByUserId(data.to.user_id);
	var sckFrom = getConnectionById(data.from);

	if (sckTo == null) {

		// pesan kalau destionation offline
		console.log('----------------------------------------------');
		console.log('receiver not found , to ' + data.to.user_id);

		// logging data history chat
		mdlHistoryChat.addHistoryChat(sckFrom.userId, sckFrom.nickName, data.to.user_id, data.to.name);

		// loggin data history chat log
		mdlHistoryChat.addHistoryChatLog(sckFrom.userId, data.to.user_id, data.message, data.message_id, data.ask_product, data.product, data.create_at, false);

		// send history to all userId connected sender
		sendLastMessageToSender(data, true);
		return false;

	}

	// logging data history chat
	mdlHistoryChat.addHistoryChat(sckFrom.userId, sckFrom.nickName, data.to.user_id, data.to.name);

	// loggin data history chat log
	mdlHistoryChat.addHistoryChatLog(sckFrom.userId, data.to.user_id, data.message, data.message_id, data.ask_product, data.product, data.create_at, true);

	// send history to all userId connected sender
	sendLastMessageToSender(data, true);
	
	// forward message to user id destination connected
	forwardTo(data, sckTo);
}

function forwardTo(data, sckTo){

	console.log('-----------------------------------------------');
	console.log('forward message from : ' + listSocketConnected[data.from].nickName + ' to ' + sckTo.nickName);
	console.log('-----------------------------------------------');

	forward = {
		response: 'get_new_message',
		success: true,
		message: '',
		data:{
			ask_product: data.ask_product,
			to: data.to,
			from: {
				userId: listSocketConnected[data.from].userId,
				name: listSocketConnected[data.from].nickName
			}, 
			message: data.message,
			message_id: data.message_id,
			create_at: data.create_at
		}
	};

	if (data.ask_product) {
		forward['data'].product = data.product;
	}		

	sendToAllConnectionHasUserId(forward, sckTo.userId);	
	console.log(forward);

}

function sendLastMessageToSender(data, isSent){

	console.log('-----------------------------------------------');
	console.log('send history Outgoing message to sender');
	console.log('-----------------------------------------------');
	
	historySender = {
		response: 'response_send_message',
		success: true,
		message:'',
		data:{
			is_sent: isSent,
			ask_product: data.ask_product,
			to: data.to,
			from: {
				userId: listSocketConnected[data.from].userId,
				name: listSocketConnected[data.from].nickName
			}, 
			message: data.message,
			message_id: data.message_id,
			create_at: data.create_at
		}
	};

	if (data.ask_product) {
		historySender['data'].product = data.product;
	}

	sendToAllConnectionHasUserId(historySender, listSocketConnected[data.from].userId);
	console.log(historySender);

}

function sendToAllConnectionHasUserId(response, userId){
	
	for (let key in listSocketConnected) {
		
		if(parseInt(listSocketConnected[key].userId) == parseInt(userId)){
			listSocketConnected[key].clientSocket.emit('chat_to_shop', response);						
		}

	}

	return true;

}

function sendToAllConnection(response){
	
	for (let key in listSocketConnected) {
		listSocketConnected[key].clientSocket.emit('chat_to_shop', response);					
	}

	return true;

}

function showListOnline(){

	console.log('----------------------------------------------');
	console.log('Update list user');
	console.log('----------------------------------------------');

	var jsonListOnline = {};
		
	// gnerate list	
	for (let key in listSocketConnected) {
		jsonListOnline[key] = {
			nickName : listSocketConnected[key].nickName
		};
	}

	console.log(jsonListOnline);
}

function getConnectionById(sckId){

	if(sckId in listSocketConnected){
		return listSocketConnected[sckId];
	}

	return null;
}

function getConnectionByUserId(userId){

	for (let key in listSocketConnected) {
		
		if(listSocketConnected[key].userId == userId){
			return listSocketConnected[key];				
		}

	}

	return null;
}

function isUserOnlineById(userId){
	
	let isFound = false;
	
	for (let key in listSocketConnected) {
		
		if(listSocketConnected[key].userId == userId){
			isFound = true;				
		}

	}

	if(isFound){
		
		socket.emit('chat_to_shop', { 
										response: 'response_check_user_online',
										success: true,
										message: '',
										data:{
											status: 'Online'
										}	
									});
	
	}else{
		
		socket.emit('chat_to_shop', { 
										response: 'response_check_user_online',
										success: true,
										message: '',
										data:{
											status: 'Offline'
										}	
									});
	}

}
